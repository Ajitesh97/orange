from odoo import models, fields, api, _
from datetime import date, datetime, time, timedelta
from pytz import timezone
import pytz


class AttendanceReport(models.Model):
    _name = 'attendance.report'

    year = fields.Selection([(str(y), str(y)) for y in range(2020, (datetime.now().year)+1)], 'Year')
    month = fields.Selection([
        ('1', 'January'),
        ('2', 'February'),
        ('3', 'March'),
        ('4', 'April'),
        ('5', 'May'),
        ('6', 'June'),
        ('7', 'July'),
        ('8', 'August'),
        ('9', 'September'),
        ('10', 'October'),
        ('11', 'November'),
        ('12', 'December'),
    ], string='Month')

    def print_attendance_report(self):
        data = {'year': self.year, 'month': self.month}
        return self.env.ref('ori_hr_attendance.attendance_summary_pdf_report').report_action([], data=data)
