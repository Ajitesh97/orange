from odoo import fields, models, api, _
from datetime import datetime
from datetime import date, timedelta, datetime


class SampleReportPrint(models.AbstractModel):
    _name = 'report.ori_hr_attendance.attendance_summary_report_pdf'

    def get_month_days(self, year, month):
        counter = datetime(int(year), int(month), 1)
        date_list = []
        while int(month) == counter.month:
            date_list.append(counter)
            counter += timedelta(days=1)
        return date_list

    @api.model
    def _get_report_values(self, docids, data):
        date_ranges_list = self.get_month_days(data['year'], data['month'])
        date_list = []
        all_date_list = []
        for record in date_ranges_list:
            date_dict = {
                'date': record.strftime('%d %b %Y'),
                'day': record.strftime('%a')
            }
            date_list.append(date_dict)
            all_date_list.append(record.strftime('%Y-%m-%d'))
        employees_list = []
        for employee in self.env['hr.employee'].search([]):
            status_list = []
            for d_l in all_date_list:
                attendances = self.env['attendance.summary'].search([('employee_id', '=', employee.id),
                                                                     ('attendance_date', '=', d_l)], limit=1)
                status = ''
                if attendances:
                    if attendances.status:
                        if attendances.status == 'Present':
                            status = 'P'
                        elif attendances.status == 'Absent':
                            status = 'A'
                        elif attendances.status == 'Week Off':
                            status = 'WO'
                        elif attendances.status == 'Half Day':
                            status = 'H'
                        else:
                            words = attendances.status.split()
                            letters = [word[0] for word in words]
                            status = "".join(letters)
                status_dict = {'status': status}
                status_list.append(status_dict)
            attendance_dict = {
                'employee': employee.name,
                'status_list': status_list,
            }
            employees_list.append(attendance_dict)
        return {
            'date_list': date_list,
            'employees_list': employees_list,
        }
