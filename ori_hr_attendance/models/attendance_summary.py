from odoo import models, fields, api, _
from datetime import date, datetime, time, timedelta
from pytz import timezone
import pytz


class AttendanceSummary(models.Model):
    _name = 'attendance.summary'
    _order = 'create_date desc'

    employee_id = fields.Many2one('hr.employee', string="Employee")
    check_in = fields.Datetime('Check In')
    check_out = fields.Datetime('Check Out')
    attendance_date = fields.Date('Date')
    status = fields.Char('Status')
    worked_hours = fields.Float('Work hours', compute='_compute_worked_hours')
    attendance_id = fields.Many2one('hr.attendance')

    @api.depends('check_in', 'check_out')
    def _compute_worked_hours(self):
        for attendance in self:
            if attendance.check_out and attendance.check_in:
                delta = attendance.check_out - attendance.check_in
                attendance.worked_hours = delta.total_seconds() / 3600.0
            else:
                attendance.worked_hours = False
