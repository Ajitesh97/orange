from odoo import models, fields, api, _


class ResourceCalendar(models.Model):
    _inherit = 'resource.calendar'

    delay_till = fields.Integer("Delay Till")
