from odoo import models, fields, api, _
from datetime import date, datetime, time, timedelta
from pytz import timezone
import pytz


class HrAttendance(models.Model):
    _name = 'hr.attendance'
    _inherit = ['hr.attendance', 'mail.thread', 'mail.activity.mixin']

    check_in = fields.Datetime(string="Check In", default=None, required=False)
    late_check_in = fields.Boolean("Late Check In", default=False)
    status = fields.Selection([
        ('waiting_for_approval', 'Waiting for Approval'),
        ('approved', 'Approved'),
        ('reject', 'Reject')
    ], string='Status')
    attendance_date = fields.Date("Date")

    @api.model
    def create(self, vals):
        res = super(HrAttendance, self).create(vals)
        if res.check_in is not False and res.check_out is False:
            self.env['attendance.summary'].create({
                'employee_id': res.employee_id.id,
                'check_in': res.check_in,
                'attendance_date': datetime.now().date(),
                'status': 'Half Day',
                'attendance_id': res.id
            })
        return res

    def button_approve(self):
        if self.employee_id.resource_calendar_id:
            week_number = datetime.today().weekday()
            week_line = self.employee_id.resource_calendar_id.attendance_ids.filtered(
                lambda s: s.dayofweek == str(week_number) and s.day_period == 'morning')
            if week_line:
                week_hour = str(week_line.hour_from).split('.')[0]
                week_minute = str(week_line.hour_from).split('.')[1]
                office_check_in = datetime.now().replace(hour=int(week_hour), minute=int(week_minute),
                                                         second=0)
                self.update({'check_in': self.convert_TZ_UTC(office_check_in.strftime('%Y-%m-%d %H:%M:%S'))})
                summary_record = self.env['attendance.summary'].search([('attendance_id', '=', self.id)])
                if summary_record:
                    required_hours = self.employee_id.resource_calendar_id.hours_per_day
                    if required_hours > self.worked_hours:
                        summary_record.status = 'Half Day'
                        summary_record.check_in = self.check_in
                        summary_record.check_out = self.check_out
                    else:
                        summary_record.status = 'Present'
                        summary_record.check_in = self.check_in
                        summary_record.check_out = self.check_out
        activity = self.env['mail.activity'].search([('res_id', '=', self.id),
                                                     ('res_model', '=', 'hr.attendance'),
                                                     ('activity_type_id', '=',
                                                      self.env.ref('ori_hr_attendance.attendance_approval').id)])
        message = 'Approved by ' + str(self.env.user.partner_id.name)
        for rec in activity:
            rec._action_done(feedback=message, attachment_ids=False)
        self.status = 'approved'

    def button_reject(self):
        activity = self.env['mail.activity'].search([('res_id', '=', self.id),
                                                     ('res_model', '=', 'hr.attendance'),
                                                     ('activity_type_id', '=',
                                                      self.env.ref('ori_hr_attendance.attendance_approval').id)])
        message = 'Rejected by ' + str(self.env.user.partner_id.name)
        for rec in activity:
            rec._action_done(feedback=message, attachment_ids=False)
        self.status = 'reject'

    def convert_TZ_UTC(self, TZ_datetime):
        fmt = "%Y-%m-%d %H:%M:%S"
        # Current time in UTC
        now_utc = datetime.now(timezone('UTC'))
        # Convert to current user time zone
        now_timezone = now_utc.astimezone(timezone(self.env.user.tz))
        UTC_OFFSET_TIMEDELTA = datetime.strptime(now_utc.strftime(fmt), fmt) - datetime.strptime(
            now_timezone.strftime(fmt), fmt)
        local_datetime = datetime.strptime(TZ_datetime, fmt)
        result_utc_datetime = local_datetime + UTC_OFFSET_TIMEDELTA
        return result_utc_datetime.strftime(fmt)

    def update_attendance_checkout(self):
        attendance_record = self.env['hr.attendance'].search([]).filtered(lambda a: a.check_out is False)
        if attendance_record:
            for records in attendance_record:
                records.check_out = self.convert_TZ_UTC(records.check_in.strftime('%Y-%m-%d 23:59:59'))
                summary_record = self.env['attendance.summary'].search([('attendance_id', '=', records.id)])
                if summary_record:
                    required_hours = records.employee_id.resource_calendar_id.hours_per_day
                    if required_hours > records.worked_hours:
                        summary_record.status = 'Half Day'
                        summary_record.check_out = records.check_out
                    else:
                        summary_record.status = 'Present'
                        summary_record.check_out = records.check_out

    def attendance_update_status(self):
        attendance_obj = self.env['hr.attendance']
        summary_obj = self.env['attendance.summary']
        for employee in self.env['hr.employee'].search([]):
            for record in attendance_obj.search([('employee_id', '=', employee.id)], order='create_date desc',
                                                limit=1):
                if record:
                    yesterday_date = datetime.now().date() - timedelta(days=1)
                    summary_record = summary_obj.search([('employee_id', '=', employee.id),
                                                         ('attendance_date', '=', yesterday_date)])
                    if not summary_record:
                        if record.check_in:
                            date_attendance = record.check_in
                        else:
                            date_attendance = record.attendance_date
                        if date_attendance.strftime('%Y-%m-%d') != yesterday_date:
                            leave = self.env['hr.leave'].search([('request_date_from', '>=', yesterday_date),
                                                                 ('request_date_to', '<=', yesterday_date),
                                                                 ('state', '=', 'validate')])
                            if leave:
                                summary_obj.create({
                                    'employee_id': employee.id,
                                    'status': leave.holiday_status_id.name,
                                    'attendance_date': yesterday_date,
                                })
                            else:
                                week_number = datetime.today().weekday()
                                week_line = employee.resource_calendar_id.attendance_ids.filtered(
                                    lambda s: s.dayofweek == str(week_number))
                                if week_line:
                                    summary_obj.create({
                                        'employee_id': employee.id,
                                        'status': 'Absent',
                                        'attendance_date': yesterday_date,
                                    })
                                else:
                                    summary_obj.create({
                                        'employee_id': employee.id,
                                        'status': 'Week Off',
                                        'attendance_date': yesterday_date,
                                    })
