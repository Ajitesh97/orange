from odoo import models, fields, api, _
from datetime import date, datetime, time, timedelta
from pytz import timezone
import pytz


class HrEmployee(models.Model):
    _inherit = 'hr.employee'

    def _get_attendance_manager_id_domain(self):
        group = self.env.ref('hr_attendance.group_hr_attendance_user', raise_if_not_found=False)
        return [('groups_id', 'in', [group.id])] if group else []

    attendance_manager_id = fields.Many2one('res.users', 'Attendance', domain=_get_attendance_manager_id_domain)

    def _attendance_action_change(self):
        res = super(HrEmployee, self)._attendance_action_change()
        manager = self.env['res.users'].search([('groups_id', 'in', self.env.ref('base.user_admin').id)], limit=1)
        if res.employee_id.attendance_manager_id:
            manager = res.employee_id.attendance_manager_id
        if res.employee_id.resource_calendar_id:
            week_number = datetime.today().weekday()
            week_line = res.employee_id.resource_calendar_id.attendance_ids.filtered(
                lambda s: s.dayofweek == str(week_number) and s.day_period == 'morning')
            if week_line:
                week_hour = str(week_line.hour_from).split('.')[0]
                week_minute = str(week_line.hour_from).split('.')[1]
                office_check_in = datetime.now().replace(hour=int(week_hour), minute=int(week_minute),
                                                         second=0) + timedelta(
                    minutes=res.employee_id.resource_calendar_id.delay_till)
                user_tz = self.env.user.tz or pytz.utc
                local = pytz.timezone(user_tz)
                check_in_datetime = datetime.strftime(pytz.utc.localize(
                    datetime.strptime(res.check_in.strftime('%Y-%m-%d %H:%M:%S'), '%Y-%m-%d %H:%M:%S')).astimezone(
                    local), "%Y-%m-%d %H:%M:%S")
                if datetime.strptime(check_in_datetime, '%Y-%m-%d %H:%M:%S') > office_check_in:
                    res.late_check_in = True
                    activity_dict = {
                        'res_model': 'hr.attendance',
                        'res_model_id': self.env.ref('hr_attendance.model_hr_attendance').id,
                        'res_id': res.id,
                        'activity_type_id': self.env.ref('ori_hr_attendance.attendance_approval').id,
                        'date_deadline': datetime.now().date(),
                        'summary': 'Attendance Approval',
                        'user_id': manager.id
                    }
                    res.env['mail.activity'].create(activity_dict)
                    res.status = 'waiting_for_approval'
        if res.check_in and res.check_out:
            summary_record = self.env['attendance.summary'].search([('attendance_id', '=', res.id)])
            if summary_record:
                required_hours = res.employee_id.resource_calendar_id.hours_per_day
                if required_hours > res.worked_hours:
                    summary_record.status = 'Half Day'
                    summary_record.check_out = res.check_out
                else:
                    summary_record.status = 'Present'
                    summary_record.check_out = res.check_out
        return res
