# -*- coding: utf-8 -*-
{
    'name': 'Hr Attendance',
    'version': '14.0',
    'category': '',
    'description': """ """,
    'depends': ['hr', 'hr_attendance', 'resource'],
    'data': [
        'security/ir.model.access.csv',
        'data/activity_data.xml',
        'wizard/attendance_report_wiz_view.xml',
        'views/employee_view.xml',
        'views/resource_calendar_view.xml',
        'views/hr_attendance_view.xml',
        'views/attendance_status_update.xml',
        'views/checkout_schedular.xml',
        'views/attendance_summary_view.xml',
        'report/attendance_report.xml',
        'report/report_paper_format.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}