# -*- coding: utf-8 -*-
{
    'name': 'HR Management',
    'version': '14.0',
    'category': 'HR management',
    'description': """ """,
    'depends': ['base', 'hrms_dashboard', 'hr_employee_transfer',
                'hr_payroll_account_community', 'hr', 'hr_expense', 'hr_holidays', 'hr_recruitment'],
    'data': [
        'security/ir.model.access.csv',
        'views/hr_apps_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}