# -*- coding: utf-8 -*-
{
    'name': 'Settings',
    'version': '14.0',
    'category': '',
    'description': """ """,
    'depends': ['base'],
    'data': [
        'views/app_menu.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}