

{
    'name': "Odoo Debranding",
    'version': "14",
    'summary': """Odoo Backend and Front end Debranding""",
    'description': """Debrand Odoo,Debranding, odoo14""",
    'category': 'Tools',
    'depends': ['website', 'base_setup'],
    'data': [
        'views/views.xml',
        'views/res_config_views.xml',
        'views/ir_module_views.xml'
    ],
    'qweb': ["static/src/xml/base.xml",
             "static/src/xml/res_config_edition.xml"],
    'images': ['static/description/banner.gif'],
    'license': "AGPL-3",
    'installable': True,
    'application': False
}
