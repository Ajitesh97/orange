# -*- coding: utf-8 -*-
{
    'name': 'CRM',
    'version': '14.0',
    'category': '',
    'description': """ """,
    'depends': ['crm'],
    'data': [
        'views/crm_view.xml'
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}