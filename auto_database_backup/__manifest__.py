{
    'name': "Automatic Database Backup",
    'version': '14.0.1.0.0',
    'summary': """Generate automatic backup of databases and store to local, google drive or remote server""",
    'description': """This module has been developed for creating database backups automatically 
                    and store it to the different locations.""",
    'author': "Cybrosys Techno Solutions",
    'website': "https://www.cybrosys.com",
    'company': 'Cybrosys Techno Solutions',
    'maintainer': 'Cybrosys Techno Solutions',
    'category': 'Tools',
    'depends': ['base', 'mail', 'google_drive'],
    'data': [
        'security/ir.model.access.csv',
        'data/data.xml',
        'views/db_backup_configure_views.xml'
    ],
    'license': 'LGPL-3',
    'images': ['static/description/banner.gif'],
    'installable': True,
    'auto_install': False,
    'application': False,
}
