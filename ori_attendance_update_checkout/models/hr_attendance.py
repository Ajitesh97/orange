from odoo import models, fields, api, _
from pytz import timezone
import pytz
from datetime import date, datetime, time


class HrAttendance(models.Model):
    _inherit = 'hr.attendance'

    def convert_TZ_UTC(self, TZ_datetime):
        fmt = "%Y-%m-%d %H:%M:%S"
        # Current time in UTC
        now_utc = datetime.now(timezone('UTC'))
        # Convert to current user time zone
        now_timezone = now_utc.astimezone(timezone(self.env.user.tz))
        UTC_OFFSET_TIMEDELTA = datetime.strptime(now_utc.strftime(fmt), fmt) - datetime.strptime(
            now_timezone.strftime(fmt), fmt)
        local_datetime = datetime.strptime(TZ_datetime, fmt)
        result_utc_datetime = local_datetime + UTC_OFFSET_TIMEDELTA
        return result_utc_datetime.strftime(fmt)

    def update_attendance_checkout(self):
        attendance_record = self.env['hr.attendance'].search([]).filtered(lambda a: a.check_out is False)
        if attendance_record:
            for records in attendance_record:
                records.check_out = self.convert_TZ_UTC(records.check_in.strftime('%Y-%m-%d 23:59:59'))
