# -*- coding: utf-8 -*-
{
    'name': 'Attendance Checkout Update',
    'version': '14.0',
    'category': '',
    'description': """ """,
    'depends': ['hr', 'hr_attendance'],
    'data': [
        'views/checkout_schedular.xml',
    ],
    'installable': True,
    'application': True,
    'auto_install': False,
}